--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.6
-- Dumped by pg_dump version 9.5.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO admin;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO admin;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO admin;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO admin;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO admin;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO admin;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: blog_comment; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE blog_comment (
    id integer NOT NULL,
    text character varying(1000) NOT NULL,
    date_created timestamp with time zone NOT NULL,
    author_id integer NOT NULL,
    post_id integer NOT NULL
);


ALTER TABLE blog_comment OWNER TO admin;

--
-- Name: blog_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE blog_comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blog_comment_id_seq OWNER TO admin;

--
-- Name: blog_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE blog_comment_id_seq OWNED BY blog_comment.id;


--
-- Name: blog_myuser; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE blog_myuser (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    date_joined date NOT NULL,
    first_name character varying(20),
    last_name character varying(30),
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL
);


ALTER TABLE blog_myuser OWNER TO admin;

--
-- Name: blog_myuser_groups; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE blog_myuser_groups (
    id integer NOT NULL,
    myuser_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE blog_myuser_groups OWNER TO admin;

--
-- Name: blog_myuser_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE blog_myuser_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blog_myuser_groups_id_seq OWNER TO admin;

--
-- Name: blog_myuser_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE blog_myuser_groups_id_seq OWNED BY blog_myuser_groups.id;


--
-- Name: blog_myuser_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE blog_myuser_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blog_myuser_id_seq OWNER TO admin;

--
-- Name: blog_myuser_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE blog_myuser_id_seq OWNED BY blog_myuser.id;


--
-- Name: blog_myuser_user_permissions; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE blog_myuser_user_permissions (
    id integer NOT NULL,
    myuser_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE blog_myuser_user_permissions OWNER TO admin;

--
-- Name: blog_myuser_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE blog_myuser_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blog_myuser_user_permissions_id_seq OWNER TO admin;

--
-- Name: blog_myuser_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE blog_myuser_user_permissions_id_seq OWNED BY blog_myuser_user_permissions.id;


--
-- Name: blog_post; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE blog_post (
    id integer NOT NULL,
    title character varying(100) NOT NULL,
    text text NOT NULL,
    date_created timestamp with time zone NOT NULL,
    author_id integer NOT NULL
);


ALTER TABLE blog_post OWNER TO admin;

--
-- Name: blog_post_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE blog_post_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blog_post_id_seq OWNER TO admin;

--
-- Name: blog_post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE blog_post_id_seq OWNED BY blog_post.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO admin;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO admin;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO admin;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO admin;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO admin;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO admin;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO admin;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_comment ALTER COLUMN id SET DEFAULT nextval('blog_comment_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_myuser ALTER COLUMN id SET DEFAULT nextval('blog_myuser_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_myuser_groups ALTER COLUMN id SET DEFAULT nextval('blog_myuser_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_myuser_user_permissions ALTER COLUMN id SET DEFAULT nextval('blog_myuser_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_post ALTER COLUMN id SET DEFAULT nextval('blog_post_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add Пользователь	1	add_myuser
2	Can change Пользователь	1	change_myuser
3	Can delete Пользователь	1	delete_myuser
4	Can add Публикация	2	add_post
5	Can change Публикация	2	change_post
6	Can delete Публикация	2	delete_post
7	Can add Комментарий	3	add_comment
8	Can change Комментарий	3	change_comment
9	Can delete Комментарий	3	delete_comment
10	Can add log entry	4	add_logentry
11	Can change log entry	4	change_logentry
12	Can delete log entry	4	delete_logentry
13	Can add group	5	add_group
14	Can change group	5	change_group
15	Can delete group	5	delete_group
16	Can add permission	6	add_permission
17	Can change permission	6	change_permission
18	Can delete permission	6	delete_permission
19	Can add content type	7	add_contenttype
20	Can change content type	7	change_contenttype
21	Can delete content type	7	delete_contenttype
22	Can add session	8	add_session
23	Can change session	8	change_session
24	Can delete session	8	delete_session
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('auth_permission_id_seq', 24, true);


--
-- Data for Name: blog_comment; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY blog_comment (id, text, date_created, author_id, post_id) FROM stdin;
1	Котик точно не пострадал?	2017-05-14 12:25:45.996202+07	3	1
2	А я вот не люблю котов.	2017-05-14 12:26:20.929787+07	4	1
3	ахххххахха!	2017-05-14 12:26:46.41062+07	2	2
4	Терпеть не могу котов!	2017-05-14 12:27:07.316702+07	4	2
5	А что же теперь делать?	2017-05-14 12:27:37.073505+07	2	4
6	Говорят, последние обновления Windows pащищают от заражения.	2017-05-14 12:28:29.746272+07	1	4
7	Спасибо, проверю!	2017-05-14 12:29:45.679463+07	2	4
8	Интересно!	2017-05-14 12:30:53.593487+07	1	1
\.


--
-- Name: blog_comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('blog_comment_id_seq', 8, true);


--
-- Data for Name: blog_myuser; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY blog_myuser (id, password, last_login, is_superuser, username, email, date_joined, first_name, last_name, is_staff, is_active) FROM stdin;
1	pbkdf2_sha256$36000$gbTnVCkA1Hfn$LQw2XuNB+ZrgKeoBVo/tV5sIldl0keB3907kpjS5gus=	2017-05-14 12:07:36.121153+07	t	admin	admin@ad.com	2017-05-14	\N	\N	t	t
2	pbkdf2_sha256$36000$uVVCV6nctz2N$nqqQqTEJWMKTiJjEDOqBqbuu+mgHAcIAGVB/k2FCJgA=	\N	f	firstuser	first@user.com	2017-05-14	Первый	Пользователь	f	t
4	pbkdf2_sha256$36000$osnjhdSfRgnM$eQdHnxvdHAB+bnk8QQfi5lSurixP+NJ6K/DiWnbg+EU=	\N	f	rude_commentator	rude@co.com	2017-05-14	\N	\N	f	t
3	pbkdf2_sha256$36000$2SEgA4nVa4sK$GCBm/I5G+/rfYwmq29ITXN/f/QR3ULh8i3khoUu6rY8=	2017-05-14 12:33:23.130469+07	f	meow	meow@me.com	2017-05-14	Кот	\N	f	t
\.


--
-- Data for Name: blog_myuser_groups; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY blog_myuser_groups (id, myuser_id, group_id) FROM stdin;
\.


--
-- Name: blog_myuser_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('blog_myuser_groups_id_seq', 1, false);


--
-- Name: blog_myuser_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('blog_myuser_id_seq', 4, true);


--
-- Data for Name: blog_myuser_user_permissions; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY blog_myuser_user_permissions (id, myuser_id, permission_id) FROM stdin;
\.


--
-- Name: blog_myuser_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('blog_myuser_user_permissions_id_seq', 1, false);


--
-- Data for Name: blog_post; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY blog_post (id, title, text, date_created, author_id) FROM stdin;
1	Кот Шредингера: суть простыми словами	<p>Наверняка вы не раз слышали, что существует такой феномен, как &laquo;Кот Шредингера&raquo;. Но если вы не физик, то, скорее всего, &nbsp;лишь отдаленно &nbsp;представляете себе, что это за кот и зачем он нужен.</p>\r\n<p>&laquo;<strong>Кот Шредингера</strong>&raquo; &ndash; так называется знаменитый мысленный эксперимент знаменитого австрийского физика-теоретика Эрвина Шредингера, который также является лауреатом Нобелевской премии. С помощью этого вымышленного опыта ученый хотел показать неполноту квантовой механики при переходе от субатомных систем к макроскопическим системам.</p>\r\n<p>В данной статье дана попытка объяснить простыми словами суть теории Шредингера про кота и квантовую механику, так чтобы это было доступно человеку, не имеющему высшего технического образования. В статье также будут представлены различные интерпретации эксперимента, в том числе и из сериала &laquo;Теория большого взрыва&raquo;.</p>\r\n<h2 id="1">Описание эксперимента</h2>\r\n<p>Оригинальная статья Эрвина Шредингера вышла в свет 1935 году. В ней эксперимент был описан с использованием <a href="http://4brain.ru/oratorskoe-iskusstvo/oratorskie-priemy.php" target="_blank">приема сравнение</a> или даже олицетворение:</p>\r\n<p><em>Можно построить и случаи, в которых довольно бурлеска. Пусть какой-нибудь кот заперт в стальной камере вместе со следующей дьявольской машиной (которая должна быть независимо от &nbsp;вмешательства кота): внутри счётчика Гейгера находится крохотное количество радиоактивного вещества, столь небольшое , что в течение часа может распасться только один атом, но с такой же вероятностью может и не распасться; если же это случится, считывающая трубка разряжается и срабатывает реле, спускающее молот, который разбивает колбочку с синильной кислотой.</em></p>\r\n<p><em>Если на час предоставить всю эту систему самой себе, то можно сказать, что кот будет жив по истечении этого времени, коль скоро распада атома не произойдёт. Первый же распад атома отравил бы кота. Пси-функция системы в целом будет выражать это, смешивая в себе или размазывая живого и мёртвого кота (простите за выражение) в равных долях. Типичным в подобных случаях является то, что неопределённость, первоначально ограниченная атомным миром, преобразуется в макроскопическую неопределённость, которая может быть устранена путём прямого наблюдения. Это мешает нам наивно принять &laquo;модель размытия&raquo; как отражающую действительность. Само по себе это не означает ничего неясного или противоречивого. Есть разница между нечётким или расфокусированным фото и снимком облаков или тумана</em></p>\r\n<p>Другими словами:</p>\r\n<ol>\r\n<li>Есть ящик и кот. В ящике имеется механизм, содержащий радиоактивное атомное ядро и ёмкость с ядовитым газом. Параметры эксперимента подобраны так, что вероятность распада ядра за 1 час составляет 50%. Если ядро распадается, открывается ёмкость с газом и кот погибает. Если распада ядра не происходит &mdash; кот остается жив-здоров.</li>\r\n<li>Закрываем кота в ящик, ждём час и задаёмся вопросом: жив ли кот или мертв?</li>\r\n<li>Квантовая же механика как бы говорит нам, что атомное ядро (а следовательно и кот) находится во всех возможных состояниях одновременно (см. квантовая суперпозиция). До того как мы открыли ящик, система &laquo;кот&mdash;ядро&raquo; находится в состоянии &laquo;ядро распалось, кот мёртв&raquo; с вероятностью 50% и в состоянии &laquo;ядро не распалось, кот жив&raquo; с вероятностью 50%. Получается, что кот, сидящий в ящике, и жив, и мёртв одновременно.</li>\r\n<li>Согласно современной копенгагенской интерпретации, кот-таки жив/мёртв без всяких промежуточных состояний. А выбор состояния распада ядра происходит не в момент открытия ящика, а ещё когда ядро попадает в детектор. Потому что редукция волновой функции системы &laquo;кот&mdash;детектор-ядро&raquo; не связана с человеком-наблюдателем ящика, а связана с детектором-наблюдателем ядра.</li>\r\n</ol>\r\n<h2 id="2">Объяснение простыми словами</h2>\r\n<p>Согласно квантовой механике, если над ядром атома не производится наблюдение, то его состояние описывается смешением двух состояний &mdash; распавшегося ядра и нераспавшегося ядра, следовательно, кот, сидящий в ящике и олицетворяющий ядро атома, и жив, и мёртв одновременно. Если же ящик открыть, то экспериментатор может увидеть только какое-нибудь одно конкретное состояние &mdash; &laquo;ядро распалось, кот мёртв&raquo; или &laquo;ядро не распалось, кот жив&raquo;.</p>\r\n<p>Вопрос стоит так: когда система перестаёт существовать как смешение двух состояний и выбирает одно конкретное? Цель эксперимента &mdash; показать, что квантовая механика неполна без некоторых правил, которые указывают, при каких условиях происходит коллапс волновой функции, и кот либо становится мёртвым, либо остаётся живым, но перестаёт быть смешением того и другого. Поскольку ясно, что кот обязательно должен быть либо живым, либо мёртвым (не существует состояния, промежуточного между жизнью и смертью), то это будет аналогично и для атомного ядра. Оно обязательно должно быть либо распавшимся, либо нераспавшимся</p>\r\n<p>&nbsp;</p>\r\n<h2>Остался ли кот живым в результате эксперимента?</h2>\r\n<p>Для тех, кто невнимательно читал статью, но все равно переживает за кота &mdash; хорошие новости: не переживайте, по нашим данным, в результате мысленного эксперимента сумасшедшего австрийского физика</p>\r\n<p align="center"><strong><em>НИ ОДИН КОТ НЕ ПОСТРАДАЛ</em></strong></p>	2017-05-14 12:13:00.521019+07	2
2	Про котиков	<p><strong>С</strong>осед:<br /> &mdash; Мы наконец-то приучили своего кота делать делишки на газету&hellip;<br /> &mdash; Хорошо бы еще научить его ждать, пока мы ее дочитаем!</p>\r\n<p><strong>С</strong>удья:<br /> &ndash; В чем вы обвиняете своего мужа?<br /> &ndash; Он назвал меня киской.<br /> &ndash; Ну и что?<br /> &ndash; А потом решил проверить, сбросив с третьего этажа, упаду ли я на все четыре лапы!</p>\r\n<p><strong>Т</strong>олько два вида живых существа на Земле способны использовать любовь к себе в корыстных целях &ndash; это женщины и кошки!</p>\r\n<p><strong>С</strong>емья поехала в деревню к родственникам. Там у кошки маленькие котята. Четырёхлетняя городская гостья взяла над ними шефство.<br /> Вечером забегает в дом и обеспокоенно кричит с порога:<br /> &ndash; Папочка, там один котёнок всю мордочку в молоке испачкал! Он теперь грязный будет ходить?<br /> Отец, лениво:<br /> &ndash; Его мама вылижет.<br /> Ребёнок:<br /> &ndash; Мам, вылижешь?!</p>\r\n<p><strong>С</strong>емья Сидоровых гуляла в парке, увидели котёнка на дереве.<br /> Весь день старались, хотели ему помочь. И смастерили скворечник!</p>\r\n<p><strong>С</strong>крестили кролика и кота. Он также охотится за мышами, но уже совсем с другой целью!</p>\r\n<p><strong>Р</strong>азговор в зоомагазине:<br /> &ndash; У вас не найдется маленькой акулы?<br /> &ndash; Для чего она вам? &ndash; удивляется продавец.<br /> &ndash; Хочу, наконец-то проучить своего кота. Сегодня он в пятый раз сожрал рыбок в аквариуме!</p>\r\n<p><strong>Р</strong>азговор двух приятелей:<br /> &ndash; Прикинь, у меня дома мышь над кошкой издевается.<br /> &ndash; Как это?<br /> &ndash; Да сыр в мышеловке валерьянкой мажет!</p>\r\n<p><strong>П</strong>о телефону:<br /> &ndash; Привет! Как дела?<br /> &ndash; Нормально. Кот спит без задних ног, я жарю окорочка.<br /> &ndash; Живодёр!</p>\r\n<p><strong>Р</strong>азговор приятелей:<br /> &ndash; У тебя кошка дома?<br /> &ndash; Да, спит на подоконнике.<br /> &ndash; А у меня уже неделю дома не появляется.<br /> &ndash; Да все они такие кошки.<br /> &ndash; Ты про кого говоришь?<br /> &ndash; Про кошку. А ты?<br /> &ndash; Я про свою жену.</p>	2017-05-14 12:16:56.066608+07	3
3	Трамп заявил о желании знать правду о хакерских атаках перед выборами в США	<div class="b-text clearfix js-topic__text">\r\n<p>Президент США Дональд Трамп заявил о желании знать правду о хакерских атаках перед выборами в США в 2016 году. Об этом он сказал в интервью, которое было показано в эфире телеканала Fox News в субботу, 13 мая, сообщает <a class="source" href="http://itar-tass.com/" target="_blank">ТАСС</a>.</p>\r\n<p>&laquo;Если хакерские атаки были, то кто их совершил? Я хочу знать правду&raquo;, &mdash; сказал Трамп, говоря о разбирательстве по делу о предполагаемом вмешательстве в президентские выборы, состоявшиеся в 2016 году. Лидер США заявил, что &laquo;хочет добраться до подноготной&raquo; происшедшего во время предвыборной кампании и не собирается мешать расследованию, которое ведут палата представителей Конгресса и ФБР.</p>\r\n<p>&laquo;Я хочу, чтобы они расследовали быстро. Но еще больше я заинтересован в том, что они расследовали правильно&raquo;, &mdash; отметил президент.</p>\r\n<p>Также Трамп дал понять, что не считает нужным создание специальной комиссии по расследованию &laquo;вмешательства России&raquo; в выборы президента США. &laquo;Не думаю, что это нужно&raquo;, &mdash; сказал он, отвечая на вопрос о возможности возникновения подобной рабочей группы. Также он отметил, что его &laquo;ничего не связывает с Россией&raquo;.</p>\r\n<p>В США после выборов президента проводится <a href="https://lenta.ru/news/2017/03/08/pervie_slushania/" target="_blank">расследование</a> предполагаемого внешнего вмешательства в предвыборную кампанию. Инициаторы разбирательства, многие из которых являлись сторонниками Демократической партии и ее кандидата Хиллари Клинтон, возлагают на Россию ответственность за серию кибератак против американских политических организаций. ЦРУ, ФБР и АНБ ранее подготовили <a href="https://lenta.ru/news/2017/01/07/declassified/" target="_blank">доклад</a>, в котором утверждалось, что президент России Владимир Путин лично распорядился организовать кампанию для того, чтобы добиться победы Трампа. Москва последовательно <a href="https://lenta.ru/news/2016/10/12/kiberterroristi/" target="_blank">отвергает</a> все подобные обвинения.</p>\r\n<p>11 мая Трамп в интервью NBC опроверг &laquo;тайное взаимодействие с россиянами&raquo; и подчеркнул, что &laquo;Россия не повлияла на голосование&raquo;, однако выступил за продолжение расследования ФБР, чтобы &laquo;выяснить, были ли на выборах проблемы, связанные с Россией&raquo;. 10 мая официальный представитель Белого дома Шон Спайсер <a href="https://lenta.ru/news/2017/05/10/nunetih/" target="_blank">заявил</a>, что Трамп не возражает против расследования.</p>\r\n</div>	2017-05-14 12:18:18.438117+07	2
4	Масштабная кибератака прокатилась по всему миру	<div class="page_news_content haveselect">\r\n<div class="universal_content clearfix">\r\n<div>Как <a href="https://ria.ru/world/20170512/1494217697.html" target="_blank">сообщает</a>&nbsp;издание <strong>BBC</strong>, многие европейские и азиатские компании подверглись масштабной кибератаке. Злоумышленники использовали вредоносную программу WanaCrypt0r 2.0, которая блокирует компьютер и требует заплатить выкуп в биткоинах.</div>\r\n<div>Новый вирус, по словам специалистов, распространяется с высокой скоростью. <strong>&laquo;Лаборатория Касперского&raquo;</strong>&nbsp;зафиксировала около 45 000 попыток атак в семидесяти четырех странах по всему миру. Но наибольшее число заражений наблюдается в России.</div>\r\n<div>По сведениям СМИ, хакерской атаке подверглись сервера МВД, Департамента информационных технологий, связи и&nbsp;защиты и операторы сотовой связи. Официальный представитель МВД Ирина Волк уточнила, что зараженными оказались около тысячи компьютеров (это менее 1%). Сейчас, по ее словам, вирус локализован, а специалисты проводят работы по его уничтожению и обновлению антивирусной защиты.</div>\r\n</div>\r\n</div>	2017-05-14 12:19:55.311161+07	1
5	В Windows Store появятся Ubuntu, SUSE Linux и Fedora	<div class="page_news_content haveselect">\r\n<div class="universal_content clearfix">\r\n<div><strong>Microsoft </strong>продолжает предоставлять возможности для разработчиков на Windows 10. В частности, после выхода версии Build 2017 <a href="https://www.theverge.com/circuitbreaker/2017/5/11/15625320/ubuntu-suse-linux-fedora-windows-store-microsoft-build-2017" target="_blank">будут доступны</a> для установки непосредственно из <strong>Windows Store</strong> три самых популярных дистрибутива Linux: Ubuntu, SUSE Linux и Fedora. Это упростит запуск приложений Linux на любом устройстве с Windows 10.</div>\r\n<div class="container_wide1">\r\n<div class="pic_container">&nbsp;</div>\r\n</div>\r\n<div>Установленная&nbsp;Linux будет работать в виртуальной среде бок о бок с Windows, с теми же утилитами командной строки, которые обычно идут с полной установкой. Вдобавок Linux-дистрибутивы смогут работать с ориентированной на учащихся и студентов Windows 10S. Это может дать Microsoft дополнительное преимущество в привлечении студентов информатики, которым может потребоваться доступ к операционной системе с открытым исходным кодом для собственных разработок.</div>\r\n</div>\r\n</div>	2017-05-14 12:21:22.811567+07	1
6	Крокодилы	<p><strong>Крокоди́лы</strong> (<a title="Латинский язык" href="https://ru.wikipedia.org/wiki/%D0%9B%D0%B0%D1%82%D0%B8%D0%BD%D1%81%D0%BA%D0%B8%D0%B9_%D1%8F%D0%B7%D1%8B%D0%BA">лат.</a>&nbsp;<em><span lang="la">Crocodilia</span></em>)&nbsp;&mdash; отряд водных позвоночных (которых обычно относят к сборной группе &laquo;<a title="Пресмыкающиеся" href="https://ru.wikipedia.org/wiki/%D0%9F%D1%80%D0%B5%D1%81%D0%BC%D1%8B%D0%BA%D0%B0%D1%8E%D1%89%D0%B8%D0%B5%D1%81%D1%8F">пресмыкающихся</a>&raquo;). В рамках <a title="" href="https://ru.wikipedia.org/wiki/%D0%9A%D0%BB%D0%B0%D0%B4%D0%B8%D1%81%D1%82%D0%B8%D0%BA%D0%B0">кладистики</a> крокодилы рассматриваются как единственный выживший субклад более широкого клада <a title="Круротарзы" href="https://ru.wikipedia.org/wiki/%D0%9A%D1%80%D1%83%D1%80%D0%BE%D1%82%D0%B0%D1%80%D0%B7%D1%8B">круротарзы</a>. Из ныне живущих организмов ближайшие родственники крокодилов&nbsp;&mdash; <a title="Птицы" href="https://ru.wikipedia.org/wiki/%D0%9F%D1%82%D0%B8%D1%86%D1%8B">птицы</a> (близкие родственники или даже потомки <a title="Архозавры" href="https://ru.wikipedia.org/wiki/%D0%90%D1%80%D1%85%D0%BE%D0%B7%D0%B0%D0%B2%D1%80%D1%8B">архозавров</a>). Все нынешние крокодилы&nbsp;&mdash; полуводные хищники, использующие в пищу водных, околоводных и приходящих на водопой животных.</p>\r\n<p>Аллигатора и крокодила легче всего отличить по строению челюстей. Когда челюсти у крокодила сомкнуты, то виден большой четвёртый зуб нижней челюсти. У аллигатора же верхняя челюсть закрывает эти зубы. Также их можно отличить по форме морды: у настоящего крокодила морда острая, V-образная, у аллигатора&nbsp;&mdash; тупая, U-образная. Крокодилы считаются более прогрессивным семейством, чем аллигаторы&nbsp;&mdash; например, у них более совершенный солевой обмен. Излишек солей выводится из организма крокодила с помощью специальных солевых желез, расположенных на языке (лингвальные железы), а также с выделениями слёзных желез (&laquo;крокодиловы слёзы&raquo;). Поэтому крокодилы могут жить в солёной воде, а аллигаторы за редкими исключениями живут только в пресной.</p>	2017-05-14 12:22:37.256308+07	2
7	Программа газификации ПАО "Газпром"	<div>На территории юга Тюменской области Программа газификации регионов России ПАО &laquo;Газпром&raquo; реализуется с 2006 года, в Ямало-Ненецком и Ханты-Мансийском автономных округах &ndash; с 2008-го.&nbsp;</div>\r\n<p><br /> Программа осуществляется совместно &laquo;Газпромом&raquo; и властями субъектов Федерации. При этом &laquo;Газпром&raquo; финансирует строительство газопроводов, обеспечивающих доведение газа до населенных пунктов, а региональные власти отвечают за прокладку уличных сетей и подготовку потребителей к приему газа. <br /> </p>\r\n<div>Инвестором по Программе является ООО &laquo;Газпром межрегионгаз&raquo; (100% дочернее общество ПАО &laquo;Газпром&raquo;), представитель инвестора на территории &mdash; ООО &laquo;Газпром межрегионгаз Север&raquo;, контролирующий план-график синхронизации выполнения Программы. </div>\r\n<div>В 2006 был сдан в эксплуатацию первый объект, построенный в рамках реализации Программы, &mdash; газопровод от поселка Коммунар до села Верхне-Бешкиль Исетского района. На его первичном запуске присутствовал Дмитрий Медведев. <br /> В декабре 2007 года &mdash; межпоселковый газопровод высокого давления ГРС &laquo;Тобольск&raquo; &mdash; ГГРП &laquo;Соколовка&raquo;, повысивший надежность газоснабжения Тобольска и Тобольского района. Его протяженность составила 10 километров. </div>\r\n<div><strong>По югу Тюменской области</strong> построены и введены в эксплуатацию газопроводы в Исетском, Тобольском, Ярковском, Абатском, Упоровском, Уватском, Голышмановском и Вагайском районах.&nbsp;</div>\r\n<div>&nbsp; <br /> В 2013 году на территории региона было введено в эксплуатацию сразу несколько газопроводов:</div>\r\n<div><strong>в Ямало-Ненецком автономном округе</strong> в феврале обеспечена поставка газа на три котельные в Новом Уренгое и Коротчаево. <br /> <br /> <strong>В Ханты-Мансийском автономном округе - Югре</strong> газифицированы сразу три населенных пункта: Троица, Белогорье и Чемаши. </div>\r\n<p>Всего за время реализации программы газификации на территории региона построено 26&nbsp;газопроводов суммарной протяженностью более 300 километров. Благодаря этому газ пришел в 28 населенных пунктов, газифицированы детские сады, школы, больницы, Дома культуры, 10 котельных, около двух тысяч домовладений.</p>	2017-05-14 12:24:00.683933+07	2
9	Google представил прослойку для отделения поддержки оборудования от версий Android	<p>Компания Google <a href="https://android-developers.googleblog.com/2017/05/here-comes-treble-modular-base-for.html">анонсировала</a> модульную систему Treble, которая позволит производителям создавать универсальные компоненты поддержки оборудования, не привязанные к конкретным версиям Android и используемым в них выпускам ядра Linux. Treble существенно упростит адаптацию новых версий Android для существующих устройств и поможет наладить оперативное создание обновлённых прошивок с актуальными выпусками Android. По аналогии с тем, как сейчас Android-приложения могут работать на любых устройствах с Android, Treble позволит использовать одни и те же драйверы с различными версиями Android.</p>\r\n<p>Если ранее производитель вынужден был портировать компоненты для поддержки оборудования для ядра каждого нового выпуска, то теперь для новых выпусков можно будет использовать уже подготовленную основу, обеспечивающую взаимодействие с оборудованием. Низкоуровневые компоненты, специфичные для каждого устройства, будут выделены в отдельный слой, который будет отделён от Android OS Framework (т.е. от используемого в Android ядра Linux) и сможет развиваться независимо. Взаимодействие Android OS Framework с компонентами поддержки оборудования будет организовано через специальную прослойку, предоставляющую неизменный программный интерфейс.</p>\r\n<center><a href="https://1.bp.blogspot.com/-o2voV-Jf41M/WRVDD9b7KiI/AAAAAAAAEHg/pTnPGLgHMlgEZiOzz13UtJ_z8YyFgd_CQCLcB/s1600/image3.png"><img style="border-style: solid; border-color: #e9ead6; border-width: 15px; max-width: 100%;" title="" src="https://www.opennet.ru/opennews/pics_base/0_1494655301.png" alt="" border="0" /></a></center><center><a href="https://4.bp.blogspot.com/-pppp3yWARE4/WRVDM2dwBQI/AAAAAAAAEHk/CtluRmtLNAQDnWI4aDcLNQJba_e1fW4RgCLcB/s1600/image2.png"><img style="border-style: solid; border-color: #e9ead6; border-width: 15px; max-width: 100%;" title="" src="https://www.opennet.ru/opennews/pics_base/0_1494655322.png" alt="" border="0" /></a></center>\r\n<p>Подсистема Treble будет включена в состав следующего выпуска платформы Android, развивающегося под кодовым именем "Android O". В том числе, интерфейс Treble уже доступен в сборках Android O Developer Preview для смартфонов Pixel. Детали реализации пока не приводятся, всю необходимую информацию планируется опубликовать сразу после релиза Android O.</p>\r\n<p>Кроме того, компания Google ведёт работу с производителями оборудования и драйверов по переносу их наработок в основную кодовую базу, развиваемую в рамках репозитория <a href="https://source.android.com/">AOSP</a> (Android Open Source Project). Некоторые изменения уже переданы в AOSP компаниями Sony и Qualcomm, что позволило избавиться от необходимости постоянной адаптации своих наборов патчей к новым выпускам Android.</p>\r\n<center><a href="https://4.bp.blogspot.com/-AJcuKbDBEOc/WRVCwgQm9AI/AAAAAAAAEHc/l0rYiTJOsX0AwGhji0hquNunTt6xR_B4ACLcB/s1600/image1.png"><img style="border-style: solid; border-color: #e9ead6; border-width: 15px; max-width: 100%;" title="" src="https://www.opennet.ru/opennews/pics_base/0_1494657727.png" alt="" border="0" /></a></center>	2017-05-14 12:41:52.788609+07	3
\.


--
-- Name: blog_post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('blog_post_id_seq', 9, true);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2017-05-14 12:07:54.953596+07	2	firstuser	1	[{"added": {}}]	1	1
2	2017-05-14 12:08:21.574872+07	2	firstuser	2	[{"changed": {"fields": ["first_name", "last_name", "email"]}}]	1	1
3	2017-05-14 12:08:40.452731+07	3	meow	1	[{"added": {}}]	1	1
4	2017-05-14 12:08:58.056687+07	3	meow	2	[{"changed": {"fields": ["first_name", "email"]}}]	1	1
5	2017-05-14 12:10:15.196169+07	4	rude_commentator	1	[{"added": {}}]	1	1
6	2017-05-14 12:10:42.207532+07	4	rude_commentator	2	[{"changed": {"fields": ["email"]}}]	1	1
7	2017-05-14 12:13:00.551608+07	1	Кот Шредингера: суть простыми словами	1	[{"added": {}}]	2	1
8	2017-05-14 12:16:56.068956+07	2	Про котиков	1	[{"added": {}}]	2	1
9	2017-05-14 12:18:18.439851+07	3	Трамп заявил о желании знать правду о хакерских атаках перед выборами в США	1	[{"added": {}}]	2	1
10	2017-05-14 12:19:55.312308+07	4	Масштабная кибератака прокатилась по всему миру	1	[{"added": {}}]	2	1
11	2017-05-14 12:21:22.812914+07	5	В Windows Store появятся Ubuntu, SUSE Linux и Fedora	1	[{"added": {}}]	2	1
12	2017-05-14 12:22:37.263963+07	6	Крокодилы	1	[{"added": {}}]	2	1
13	2017-05-14 12:24:00.713772+07	7	Программа газификации ПАО "Газпром"	1	[{"added": {}}]	2	1
14	2017-05-14 12:25:46.001417+07	1	Котик точно не пострадал?	1	[{"added": {}}]	3	1
15	2017-05-14 12:26:20.930742+07	2	А я вот не люблю котов.	1	[{"added": {}}]	3	1
16	2017-05-14 12:26:46.416446+07	3	ахххххахха!	1	[{"added": {}}]	3	1
17	2017-05-14 12:27:07.317948+07	4	Терпеть не могу котов!	1	[{"added": {}}]	3	1
18	2017-05-14 12:27:37.074489+07	5	А что же теперь делать?	1	[{"added": {}}]	3	1
19	2017-05-14 12:28:29.748035+07	6	Говорят, последние обновления Windows pащищают от заражения.	1	[{"added": {}}]	3	1
20	2017-05-14 12:29:45.687608+07	7	Спасибо, проверю!	1	[{"added": {}}]	3	1
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 20, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	blog	myuser
2	blog	post
3	blog	comment
4	admin	logentry
5	auth	group
6	auth	permission
7	contenttypes	contenttype
8	sessions	session
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('django_content_type_id_seq', 8, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2017-05-14 12:06:20.401234+07
2	contenttypes	0002_remove_content_type_name	2017-05-14 12:06:20.445311+07
3	auth	0001_initial	2017-05-14 12:06:20.638467+07
4	auth	0002_alter_permission_name_max_length	2017-05-14 12:06:20.679911+07
5	auth	0003_alter_user_email_max_length	2017-05-14 12:06:20.71845+07
6	auth	0004_alter_user_username_opts	2017-05-14 12:06:20.76239+07
7	auth	0005_alter_user_last_login_null	2017-05-14 12:06:20.792382+07
8	auth	0006_require_contenttypes_0002	2017-05-14 12:06:20.796608+07
9	auth	0007_alter_validators_add_error_messages	2017-05-14 12:06:20.828351+07
10	auth	0008_alter_user_username_max_length	2017-05-14 12:06:20.866561+07
11	blog	0001_initial	2017-05-14 12:06:21.173866+07
12	admin	0001_initial	2017-05-14 12:06:21.283645+07
13	admin	0002_logentry_remove_auto_add	2017-05-14 12:06:21.339204+07
14	sessions	0001_initial	2017-05-14 12:06:21.36571+07
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('django_migrations_id_seq', 14, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
huwkh2uqwfc46scpttza9g4jcvcyhef9	YjY1MDg1OTI3YTQ3YzE3MjYyODRjZTZjNTEyMzcwNzY2MmU1NDEyMjp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlOTUxNWRmN2EyMjFiYjlhNGNlY2U1NDI3MjdhZmM5YjI0Y2EyMjFiIn0=	2017-05-28 12:33:23.136689+07
\.


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: blog_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_comment
    ADD CONSTRAINT blog_comment_pkey PRIMARY KEY (id);


--
-- Name: blog_myuser_email_key; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_myuser
    ADD CONSTRAINT blog_myuser_email_key UNIQUE (email);


--
-- Name: blog_myuser_groups_myuser_id_group_id_2a262a29_uniq; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_myuser_groups
    ADD CONSTRAINT blog_myuser_groups_myuser_id_group_id_2a262a29_uniq UNIQUE (myuser_id, group_id);


--
-- Name: blog_myuser_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_myuser_groups
    ADD CONSTRAINT blog_myuser_groups_pkey PRIMARY KEY (id);


--
-- Name: blog_myuser_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_myuser
    ADD CONSTRAINT blog_myuser_pkey PRIMARY KEY (id);


--
-- Name: blog_myuser_user_permiss_myuser_id_permission_id_f09523ed_uniq; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_myuser_user_permissions
    ADD CONSTRAINT blog_myuser_user_permiss_myuser_id_permission_id_f09523ed_uniq UNIQUE (myuser_id, permission_id);


--
-- Name: blog_myuser_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_myuser_user_permissions
    ADD CONSTRAINT blog_myuser_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: blog_myuser_username_key; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_myuser
    ADD CONSTRAINT blog_myuser_username_key UNIQUE (username);


--
-- Name: blog_post_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_post
    ADD CONSTRAINT blog_post_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON auth_permission USING btree (content_type_id);


--
-- Name: blog_comment_author_id_4f11e2e0; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX blog_comment_author_id_4f11e2e0 ON blog_comment USING btree (author_id);


--
-- Name: blog_comment_post_id_580e96ef; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX blog_comment_post_id_580e96ef ON blog_comment USING btree (post_id);


--
-- Name: blog_myuser_email_6f791413_like; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX blog_myuser_email_6f791413_like ON blog_myuser USING btree (email varchar_pattern_ops);


--
-- Name: blog_myuser_groups_group_id_18364efb; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX blog_myuser_groups_group_id_18364efb ON blog_myuser_groups USING btree (group_id);


--
-- Name: blog_myuser_groups_myuser_id_9b025807; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX blog_myuser_groups_myuser_id_9b025807 ON blog_myuser_groups USING btree (myuser_id);


--
-- Name: blog_myuser_user_permissions_myuser_id_1c6bf21a; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX blog_myuser_user_permissions_myuser_id_1c6bf21a ON blog_myuser_user_permissions USING btree (myuser_id);


--
-- Name: blog_myuser_user_permissions_permission_id_61a1da5f; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX blog_myuser_user_permissions_permission_id_61a1da5f ON blog_myuser_user_permissions USING btree (permission_id);


--
-- Name: blog_myuser_username_a1b84a26_like; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX blog_myuser_username_a1b84a26_like ON blog_myuser USING btree (username varchar_pattern_ops);


--
-- Name: blog_post_author_id_dd7a8485; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX blog_post_author_id_dd7a8485 ON blog_post USING btree (author_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX django_session_expire_date_a5c62663 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_comment_author_id_4f11e2e0_fk_blog_myuser_id; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_comment
    ADD CONSTRAINT blog_comment_author_id_4f11e2e0_fk_blog_myuser_id FOREIGN KEY (author_id) REFERENCES blog_myuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_comment_post_id_580e96ef_fk_blog_post_id; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_comment
    ADD CONSTRAINT blog_comment_post_id_580e96ef_fk_blog_post_id FOREIGN KEY (post_id) REFERENCES blog_post(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_myuser_groups_group_id_18364efb_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_myuser_groups
    ADD CONSTRAINT blog_myuser_groups_group_id_18364efb_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_myuser_groups_myuser_id_9b025807_fk_blog_myuser_id; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_myuser_groups
    ADD CONSTRAINT blog_myuser_groups_myuser_id_9b025807_fk_blog_myuser_id FOREIGN KEY (myuser_id) REFERENCES blog_myuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_myuser_user_per_myuser_id_1c6bf21a_fk_blog_myus; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_myuser_user_permissions
    ADD CONSTRAINT blog_myuser_user_per_myuser_id_1c6bf21a_fk_blog_myus FOREIGN KEY (myuser_id) REFERENCES blog_myuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_myuser_user_per_permission_id_61a1da5f_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_myuser_user_permissions
    ADD CONSTRAINT blog_myuser_user_per_permission_id_61a1da5f_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_post_author_id_dd7a8485_fk_blog_myuser_id; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY blog_post
    ADD CONSTRAINT blog_post_author_id_dd7a8485_fk_blog_myuser_id FOREIGN KEY (author_id) REFERENCES blog_myuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_c564eba6_fk_blog_myuser_id; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_blog_myuser_id FOREIGN KEY (user_id) REFERENCES blog_myuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

