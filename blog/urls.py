from django.conf.urls import url
from django.contrib.auth.views import login, logout_then_login
from blog import views

urlpatterns = [
    url(r'^$', views.MainPage.as_view(), name='main'),

    url(r'^posts/(?P<author>\w+)/(?P<pk>\d+)/$', views.DetailPost.as_view(), name='post'),
    url(r'^new_post/$', views.CreatePost.as_view(), name='create_post'),
    url(r'^delete_post/(?P<pk>\d+)/$', views.DeletePost.as_view(), name='delete_post'),
    url(r'^delete_comment/(?P<pk>\d+)/$', views.DeleteComment.as_view(), name='delete_comment'),

    url(r'^profile/(?P<username>\w+)/$', views.Profile.as_view(), name='profile'),

    url(r'^login/$', login, {'template_name': 'login.html'}, name='login'),
    url(r'^logout/$', logout_then_login, name='logout')
]