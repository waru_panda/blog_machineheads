# coding: utf-8

from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager
from django.core import validators
from django.db import models


# Create your models here.
from django.utils import timezone
from django.utils.html import format_html


class MyUser(AbstractBaseUser, PermissionsMixin):
    username = models.CharField('Логин', max_length=30, unique=True,
                                help_text='Не более 30 символов.'
                                          ' Только латинские буквы, цифры и символ подчеркивания.',
                                validators=[
                                    validators.RegexValidator(r'\w+',
                                                              'Введите правильное имя пользователя. Оно '
                                                              'должно быть на английском языке и  '
                                                              'содержать только буквы, цифры и знак подчеркивания.',
                                                              'invalid'),
                                ],
                                error_messages={
                                    'unique': 'Пользователь с таким именем уже существует.',
                                })
    email = models.EmailField('Адрес электронной почты', unique=True,
                              error_messages={
                                  'unique': 'Пользователь с такой электронной почтой уже существует.',
                              })
    date_joined = models.DateField('Дата регистрации', default=timezone.now)
    first_name = models.CharField('Имя', max_length=20, blank=True, null=True)
    last_name = models.CharField('Фамилия', max_length=30, blank=True, null=True)
    is_staff = models.BooleanField('Статус персонала', default=False,
                                   help_text=u'Отметьте, если пользователь может входить'
                                             u' в административную часть сайта.')
    is_active = models.BooleanField(u'Активный', default=True,
                                    help_text=u'Отметьте, если пользователь должен считаться активным.'
                                              u' Уберите эту отметку вместо удаления учётной записи.')
    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def __str__(self):
        return '{}'.format(self.username)

    class Meta:
        verbose_name = u'Пользователь'
        verbose_name_plural = u'Пользователи'

    def get_short_name(self):
        short_name = '{}'.format(self.username)
        return short_name

    def get_full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name


class Post(models.Model):
    author = models.ForeignKey(MyUser, verbose_name='Автор')
    title = models.CharField('Заголовок', max_length=100)
    text = models.TextField('Текст публикации',
                            validators=[validators.RegexValidator(r'.*\S.*', 'Обязательное поле', 'invalid')])
    date_created = models.DateTimeField('Дата создания', auto_now_add=True)

    def __str__(self):
        return '{}'.format(self.title)

    def get_preview(self):
        if len(self.text) > 500:
            return format_html('{}...'.format(self.text[:500]))
        else:
            return format_html(self.text)

    class Meta:
        verbose_name = 'Публикация'
        verbose_name_plural = 'Публикации'
        ordering = ('-date_created',)


class Comment(models.Model):
    author = models.ForeignKey(MyUser, verbose_name='Автор')
    post = models.ForeignKey(Post, verbose_name='Публикация')
    text = models.CharField('Текст комментария', max_length=1000,
                            validators=[validators.RegexValidator(r'.*\S.*', 'Обязательное поле', 'invalid')])
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}'.format(self.text)

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'
        ordering = ('date_created',)
