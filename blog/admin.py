from django.contrib import admin

from django.contrib.auth.admin import UserAdmin
from blog import models


@admin.register(models.MyUser)
class MyUserAdmin(UserAdmin):
    list_display = ('username', 'email', 'date_joined')


@admin.register(models.Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'date_created', 'get_preview')
    list_filter = ['date_created', 'author']
    search_fields = ['title', 'text']
    change_form_template = 'change_form.html'


@admin.register(models.Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('author', 'post', 'date_created', 'text')
