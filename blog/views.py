from django.contrib import messages
from django.http import HttpResponseRedirect

from django.urls import reverse
from django.views import generic
from django.views.generic.edit import FormMixin
from blog import models, forms


class MainPage(generic.ListView):
    model = models.Post
    template_name = 'main_page.html'


class Profile(generic.DetailView):
    model = models.MyUser
    template_name = 'profile.html'
    slug_field = 'username'
    slug_url_kwarg = 'username'


class CreatePost(generic.CreateView):
    model = models.Post
    template_name = 'posts/create_post.html'
    form_class = forms.PostForm

    def form_valid(self, form):
        post = form.save(commit=False)
        post.author = self.request.user
        post.save()
        messages.success(self.request, 'Публикация добавлена')
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('blog:main')


# FormMixin для выведения формы модели Comment одновременно с DetailView модели Post
class DetailPost(FormMixin, generic.DetailView):
    model = models.Post
    context_object_name = 'post'
    template_name = 'posts/detail_post.html'
    form_class = forms.CommentForm
    slug_field = 'author'
    slug_url_kwarg = 'author'
    object = None
    comment = None

    def get_context_data(self, **kwargs):
        context = super(DetailPost, self).get_context_data(**kwargs)
        context['form'] = self.get_form()
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        comment = form.save(commit=False)
        comment.author = self.request.user
        comment.post = self.object
        comment.save()
        messages.success(self.request, 'Комментарий добавлен')
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('blog:post', args=(self.object.author, self.object.pk))


class DeletePost(generic.DeleteView):
    model = models.Post
    template_name = 'posts/delete_post.html'

    def get_success_url(self):
        return reverse('blog:profile', args=(self.object.author,))


class DeleteComment(generic.DeleteView):
    model = models.Comment
    template_name = 'posts/delete_comment.html'

    def get_success_url(self):
        return reverse('blog:post', args=(self.object.post.author, self.object.post.pk,))
